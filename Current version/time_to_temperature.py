__author__ = 'bansal'
from building_model import *
from remote_data_fetcher import *
from datetime import date, timedelta, datetime, time
from time import time
import re

def time_to_temperature(user_room_location, filename="time2tem"):
    time_array = []
    # Step 0: Retrieve user data
    user_data = get_room_settings(user_room_location)  # it is assumed that user location will be available through some means
    set_point = extract_set_point(user_data)
    room_id_simulate = user_room_location
    # Step 1
    # Generate netlist lines
    start_t = time()
    [sim_start_point,sim_end_point] = convert_date_time(user_data)  #obtain the start and end time for simulation in the correct format. These start and end times are used for extracting relevant weather data
    [lines, res_count, volt_count, cap_count, zone_node_dict, zone_id_simulate]=building_model([room_id_simulate], sim_start_point, sim_end_point)
    time_array.append(time()-start_t)

    #Step 2
    # write the file
    start_t = time()
    netlist = netlist_class(filename, lines)
    length=len(netlist.lines)
    netlist.lines.insert(length-3,"I1 0 n1 PULSE(0 177.8 10 0 0 86100 86100 1)")  # this has to change. right now i am putting a heater source of my choice in time to temperature
    netlist.write_file()
    time_array.append(time()-start_t)

    # Step 3
    # simulate the file
    start_t =  time()
    netlist.run_sim()
    time_array.append(time()-start_t)

    # Step 4
    # convert the file
    start_t =  time()
    netlist.convert_raw("{}".format(zone_node_dict[zone_id_simulate]));
    time_array.append(time()-start_t)

    # Step 5
    # check when the temperature is reached
    start_t =  time()
    check= temperature_check("{}.txt".format(netlist.name.split(".net")[0]), set_point)
    time_array.append(time()-start_t)
    if check:
        return([check, time_array])
    else:
        return [False, time_array]

    # step 4
    #clean all other files


def temperature_check(temperature_filename, set_point):
        """Read the file's lines into a list, removing the endline characters, and placing it in the lines attribute.
            Finds parameter statements and voltage statements and places them in parameters and voltage_parameters attributes correspondingly. """
        file = open(temperature_filename,"r")
        lines = file.readlines()
        for line in lines:
            #remove trailing "\r\n" characters.
            line = line.rstrip()
            print line
            #check for parameters and voltage parameters
            temp_check = temperature_read(line)
            if temp_check != False:
                if ((float(temp_check[1]) >=set_point-0.1) and  (float(temp_check[1]) <=set_point+0.1)):
                    return ([float(temp_check[0]), float(temp_check[1])])

        return False
        file.close()

def temperature_read(line):
    """finds the parameter and value for a given line and parameter"""
    regex_string = "\S+ *; *\S+" ## Regex string to identify pattern corresponding to .param statements
    temp_regex = re.compile(regex_string,re.IGNORECASE)
    temp_find =temp_regex.match(line) ## findall(line) matches all the instances of the regex_string pattern in line
    if temp_find:
        [time, temp] = temp_find.group().split(";")
        return([time, temp])
    else:
        return False

def main():
    filename="test_time_to_temperature_2.net"
    [check, time_array]=time_to_temperature(3, filename)
    print "FINISHED"
    print "-------TIMINGS-------"
    print "Time to fetch data from server & build the model: {0:.1f}ms".format(time_array[0]*1000)
    print "-----------------------------------"
    print "Time to write netlist: {0:.1f}ms".format(time_array[1]*1000)
    print "-----------------------------------"
    print "Total LTSPICE sim: {0:.1f}ms".format(time_array[2]*1000)
    print "-----------------------------------"
    import re
    for line in open(filename[:-4]+'.log'):
        if re.findall(r'(\d+\.\d+)(?= seconds.)',line):
            coretime = float(re.findall(r'(\d+\.\d+)(?= seconds.)',line)[0])
            print "Core LTSPICE sim: {0:.1f}ms".format(coretime*1000)
            print "-----------------------------------"
            break
    print "Binary to ASCII convertion: {0:.1f}ms".format(time_array[3]*1000)
    print "-----------------------------------"
    total = 0
    for t in time_array:
        total += t
    print "Total time: {0:.1f}ms".format(total*1000)
    print "Time to check temperature: {0:.1f}ms".format(time_array[4]*1000)
    print "-----------------------------------"
    if check:
        print ("Time to reach {} will be {}".format(check[1], check[0]))
    else :
        print (" cannot achieve this temperature")
    clean_aux_files()



def extract_set_point(user_data):
    set_point = user_data[0]["fields"]["set_value"]
    return set_point

def convert_date_time(user_data):
    set_point_date = user_data[0]["fields"]["fromDay"]
    set_point_time = user_data[0]["fields"]["fromTime"]
    set_point_date=set_point_date.split("-")
    set_point_time =set_point_time.split(":")
    current_date=datetime(int(set_point_date[0]),int(set_point_date[1]), int(set_point_date[2]),int(set_point_time[0]),int(set_point_time[1]), int(set_point_time[2]) )
    sim_start_point = current_date + timedelta(days=-1)
    sim_start_point = '{:%Y%m%d%H%M%S}'.format(sim_start_point)
    sim_end_point = current_date + timedelta(days=-1,hours=12)
    sim_end_point = '{:%Y%m%d%H%M%S}'.format(sim_end_point)
    return  sim_start_point,sim_end_point

if __name__ == "__main__":
    main()