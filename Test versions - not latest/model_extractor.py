__author__ = 'shubhambansal'
from remote_data_fetcher import *

# still I need to add the ability to check if all the necessary parameters are given. For example, if three layers
# are given then we should have three values of all the coefficients
class building_data_class:

     def __init__(self,filename):
        self.name = filename
        self.read_file()
     def __repr__(self):
        return(self.name)
     def __str__(self):
        return(self.name)

     def read_file(self):
        """Read the file's lines into a list. Generate a dictionary of coefficients. each identifier of this dictionary is the corresponding building element identifier
        provided by the user. Each value of this dictionary is an object whose attributes are different coefficients and boundary conditions """
        file = open(self.name,"r")
        lines = file.readlines()[1:] ## to ignore the headers of the csv file
        coefficients = {} ## this creates an empty dictionary type to hold value of all the material coefficients
        for line in lines:
            #remove trailing "\r\n" characters.
            edited_line = line.rstrip()
            #check for coefficients
            coeff_check = coefficient_check(edited_line)
            if coeff_check != False:
                coefficients[coeff_check.identifier] = coeff_check # here we have created a dictionary
        self.coefficients =  coefficients
        file.close()

def coefficient_check(line):
    """finds all the different coefficients in the given line """
    a_param = line.split(";")
    value=[]
    identifier = a_param[0]
    area = float(a_param[2])
    cp=a_param[3].split()
    cp= [float(i) for i in cp]
    d=a_param[4].split()
    d=[float(i) for i in d]
    rho=a_param[5].split()
    rho=[float(i) for i in rho]
    t=a_param[6].split()
    t=[float(i) for i in t]
    conv_left = float(a_param[7])
    conv_right =float(a_param[8])
    win = a_param[9].split()
    win = [float(i) for i in win]
    left_voltage = a_param[10].split()
    right_voltage = a_param[11].split()
    coefficient_statement = coefficient_statement_class(identifier, area, cp, d, rho, t, conv_left, conv_right,win, left_voltage, right_voltage)
    return(coefficient_statement)

class coefficient_statement_class:
    """A container class for carrying around coefficient statement info"""
    def __init__(self,identifier,area, cp, d, rho, t, conv_left, conv_right, win,  left_voltage, right_voltage ):
        self.identifier = identifier
        self.area = area
        self.cp = cp
        self.d = d
        self.rho = rho
        self.t= t
        self.conv_left = conv_left
        self.conv_right = conv_right
        self.win=win
        self.left_voltage =  left_voltage
        self.right_voltage =  right_voltage
    def __repr__(self):
       return("{}".format(self.identifier))
    def __str__(self):
       return("{}".format(self.identifier))


## All the code below is used to convert coefficient values into corresponding electrical components

def element_dictionary(coeff_dictionary):
    """This function creates a elem_dict to store the value of different resistance, capacitance and voltage sources of all the building elements"""
    elem_dict={}
    for variable, value in coeff_dictionary.iteritems():
        element = compute(variable,value)
        elem_dict["Res_"+"{}".format(element.identifier)]=str(element.res)
        elem_dict["Cap_"+"{}".format(element.identifier)]=str(element.cap)
        elem_dict["Resconvleft_"+"{}".format(element.identifier)]=str(element.res_conv_left)
        elem_dict["Resconvright_"+"{}".format(element.identifier)]=str(element.res_conv_right)
        if (element.res_win != float('Inf')):
            elem_dict["Reswin_"+"{}".format(element.identifier)]=str(element.res_win)
        # following lines will have to be changed so as to insert room id automatically
        if (element.left_voltage_value!='vr'):
            elem_dict["{}".format(element.left_voltage_name)]= str(get_dynamic_data(roomid=int(element.left_voltage_value), f_time=20141116000000, t_time=20141116235500, res=120))
        elem_dict["{}".format(element.right_voltage_name)]=str(get_dynamic_data(roomid=int(element.right_voltage_value), f_time=20141116000000, t_time=20141116235500, res=120))
    return elem_dict


class compute:
    """ This class return a building element object with attributes such as resistance capacitance and boundary condition. These values are computed from the coefficients data"""
    def __init__(self,variable, value):
        # code to obtain the wall coefficient3
        self.win_area=value.win[0]
        area = value.area-self.win_area
        cp = (value.cp)
        d = (value.d)
        rho = (value.rho)
        t= (value.t)
        total_t= sum(value.t)
        davg = sum ([d[i]*t[i] for i in range(len(d))])/total_t
        cpavg = sum ([cp[i]*t[i] for i in range(len(d))])/total_t
        rhoavg = sum ([rho[i]*t[i] for i in range(len(d))])/total_t
        self.identifier = value.identifier
        self.res = (rhoavg*total_t)/(2*area)
        self.cap = davg*total_t*area*cpavg
        self.res_conv_left = 1/(area*value.conv_left)
        self.res_conv_right = (1/(area*value.conv_right))
        self.left_voltage_name = value.left_voltage[0]
        self.left_voltage_value = value.left_voltage[1]
        self.right_voltage_name = value.right_voltage[0]
        self.right_voltage_value = value.right_voltage[1]

        if (self.win_area <= value.area):
        # calculation for window
           if (self.win_area!=0):
                uo=(value.win[1]*value.win[2])/(value.win[1]+value.win[2])
                delta_u= value.win[1]-uo
                self.res_win=1/(self.win_area*(uo+value.win[3]*delta_u))
           else:
                self.res_win=float('Inf')
        else:
            raise ValueError("window area cannot be larger than wall area")
    def __repr__(self):
        return("electric_element")