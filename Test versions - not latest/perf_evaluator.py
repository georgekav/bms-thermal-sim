__author__ = 'Georgios Lilis'
from main import *
from time import time

if __name__ == "__main__":
    netlist_fn = "test_10.net"
    time_array = []

    #step 1
    start_t = time()
    original_netlist = fetch_netlist_file(netlist_fn)
    time_array.append(time()-start_t)

    #step 2
    start_t = time()
    change_params = extract_model("building_data.txt")
    time_array.append(time()-start_t)

    #step 3
    start_t = time()
    new_netlist = generate_new_netlist(original_netlist, change_params)
    time_array.append(time()-start_t)

    #step 4
    start_t = time()
    new_netlist.run_sim()
    time_array.append(time()-start_t)

    #step 5
    start_t = time()
    new_netlist.convert_raw("vr")
    time_array.append(time()-start_t)

    #step 6
    #clean all other files, not timed
    clean_aux_files()
    print "FINISHED"
    print "-------TIMINGS-------"
    print "Reading old netlist: {0:.1f}ms".format(time_array[0]*1000)
    print "-----------------------------------"
    print "Extracting model: {0:.1f}ms".format(time_array[1]*1000)
    print "-----------------------------------"
    print "Generating new netlist: {0:.1f}ms".format(time_array[2]*1000)
    print "-----------------------------------"
    print "Total LTSPICE sim: {0:.1f}ms".format(time_array[3]*1000)
    print "-----------------------------------"
    import re
    for line in open(netlist_fn[:-4]+'_new.log'):
        if re.findall(r'(\d+\.\d+)(?= seconds.)',line):
            coretime = float(re.findall(r'(\d+\.\d+)(?= seconds.)',line)[0])
            print "Core LTSPICE sim: {0:.1f}ms".format(coretime*1000)
            print "-----------------------------------"
            break
    print "Binary to ASCII convertion: {0:.1f}ms".format(time_array[4]*1000)
    print "-----------------------------------"
    total = 0
    for t in time_array:
        total += t
    print "Total time: {0:.1f}ms".format(total*1000)
