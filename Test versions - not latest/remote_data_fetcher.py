__author__ = 'Georgios Lilis'
import requests, logging, sys
import time
from datetime import date, timedelta, datetime, time
OPENBMS_IP = '128.178.19.163'
OPENBMS_PORT = '80'

def get_dynamic_data(roomid, f_time, t_time, res):
    """It gets the roomid the start and stop time as well as the required resolution of the samples and returns
    a string of 'voltages' for direct inclusion in .netlist. The roomid is the openBMS virtual resource id corresponding
    to the room the temperature needs to be converted in voltage list.
    f_time, t_time in human readable format %Y%m%d%H%M%S """
    geturl = 'http://{0}:{1}/API/timeseries/TEM/room/{2}/from{3}/to{4}/res{5}'\
        .format(OPENBMS_IP,OPENBMS_PORT, roomid, f_time, t_time, res)

    r = requests.get(geturl)
    if r.status_code == 200: # Otherwise it means openBMS doesnt work
        temp_array = r.json()['TEM']
        reftime = temp_array[0][0]
        voltage_array_str = '('
        for elem in temp_array:
            voltage_array_str += str(int(elem[0] - reftime))
            voltage_array_str += ' '
            voltage_array_str += str(elem[1])
            voltage_array_str += ' '
        voltage_array_str = voltage_array_str[:-1] + ')'
        return voltage_array_str
    else:
        logging.error('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
        sys.exit()

def get_static_data(unitid):
    """Gets the architecture zone and border data and creates a dictionary object according to the ETH thermal model
    in order to simulate it in LTspice. It gets as input the unitid"""
    geturl = 'http://{0}:{1}/API/zones/unit/{2}'.format(OPENBMS_IP,OPENBMS_PORT, unitid)
    r = requests.get(geturl)
    if r.status_code == 200: # Otherwise it means openBMS doesnt work
        return r.json()
    else:
        logging.error('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
        sys.exit()
    pass

def get_room_settings(roomid):
    geturl = 'http://{0}:{1}/API/room_settings/{2}'.format(OPENBMS_IP,OPENBMS_PORT, roomid)
    r=requests.get(geturl)
    if r.status_code == 200: # Otherwise it means openBMS doesnt work
        return r.json()
    else:
        logging.error('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
        sys.exit()
    pass

#Here is a test
alldata = get_static_data(1)
print alldata


alldata = get_room_settings(3)
print alldata



# alldata = get_static_data(1)
# print alldata

pass

# test = get_dynamic_data(roomid=1, f_time=20150315181230, t_time=20150315191230, res=60)  #Get one hour in minute resolution
# print test

