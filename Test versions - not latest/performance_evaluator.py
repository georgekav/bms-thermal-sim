__author__ = 'bansal'
def main():
    # Step 1
    # Generate netlist lines
    zone_id_simulate=6
    building_object=netlist_class()
    static_lines=building_object.building_model()
    dynamic_lines=building_object.zone_components([zone_id_simulate])
    print building_object.res_count;
    print building_object.volt_count;
    print building_object.cap_count;

    #Step 2
    # write the file
    filename="test_14.net";
    lines= static_lines + dynamic_lines
    building_object.name=filename
    building_object.write_file(lines);

    # Step 3
    # simulate the file
    building_object.run_sim();
    building_object.convert_raw("{}".format(building_object.zone_node_dict[zone_id_simulate]));
    print "{}".format(building_object.zone_node_dict[zone_id_simulate])

    # Step 4
    #clean all other files
    clean_aux_files()
    print "FINISHED"