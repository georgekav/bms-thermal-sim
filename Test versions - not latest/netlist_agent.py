import re
import os
import sys
import copy
import subprocess
import math

class netlist_class:
    """A class for carrying around info about a netlist file.
        Has methods for running the netlist, collecting the raw file output,
        and changing parameters of the netlist."""
    def __init__(self,filename):
        self.name = filename
        self.read_file()

    def __repr__(self):
        return(self.name)
    def __str__(self):
        return(self.name)

    def read_file(self):
        """Read the file's lines into a list, removing the endline characters, and placing it in the lines attribute.
            Finds parameter statements and voltage statements and places them in parameters and voltage_parameters attributes correspondingly. """
        file = open(self.name,"r")
        lines = file.readlines()
        edited_lines = []
        parameters = {} ## this creates an empty dictionary to store passive data of the netlist
        voltage_parameters={} ## this creates an empty dictionary to store active data (voltage) of the netlist
        line_counter = 0
        for line in lines:
            #remove trailing "\r\n" characters.
            edited_line = line.rstrip()
            edited_lines.append(edited_line)
            edited_lines.append(edited_line)
            #check for parameters and voltage parameters
            param_check = parameter_check(line)
            volt_check = voltage_check(line)
            if param_check != False:
                for a_param in param_check:
                    a_param.line_number = line_counter
                    parameters[a_param.variable] = a_param
            if volt_check != False:
                for v_param in volt_check:
                    v_param.line_number = line_counter
                    voltage_parameters[v_param.variable] = v_param

            line_counter += 1

        self.lines = edited_lines
        self.parameters = parameters #its a dictionary like this: {'Rload0': Rload0=1k, 'Vsource': Vsource=5v, 'fdc-': fdc-=56, 'zzz': zzz=46, 'Rload1': Rload1=5k}
        self.voltage_parameters=voltage_parameters
        file.close()

    def change_single_param(self,a_variable,a_value):
        """Finds the given variable in the netlist lines and replaces the current value with the given value.
            Returns the new lines of a new netlist with the value changed.
            Really only for internal use"""
        try:
            parameter_statment = self.parameters[a_variable]
            new_lines = change_parameter_value(a_param=parameter_statment,new_value=str(a_value),current_lines=self.lines) ## a function has been called that changes the value and returns new lines
        except KeyError:## incase the voltage parameter has to be changed then it wont be found in parameter dictionary and will raise an error
            volt_parameter_statement=self.voltage_parameters[a_variable]
            new_lines = change_volt_parameter_value(a_param=volt_parameter_statement,new_value=str(a_value),current_lines=self.lines)
        return(new_lines)

    def change_parameters(self,variable_value_dictionary,new_filename=None):
        """Changes all the parameters of the netlist lines to the values given in the variable_value_dictionary.
            Returns a new netlist object."""
        #Create the new_netlist from the old and rename it's by appending "new" or the given filename
        new_netlist = copy.deepcopy(self)
        if new_filename == None:
            new_netlist.name = "{}_new".format(self.name.split(".net")[0])+".net"
        else:
            new_netlist.filename = new_filename

        ##Change the parameters and update the new parameter dictionary
        new_parameters = self.parameters
        for variable, value in variable_value_dictionary.iteritems():
            try:
                new_netlist.lines = new_netlist.change_single_param(variable,value)
            except KeyError as keyerror:
                print variable
                print(ParameterException(keyerror.args[0]))
                print variable
                sys.exit(0)
            new_parameter_statement = parameter_statement_class(variable,value)
            new_parameters[variable] = new_parameter_statement
        new_netlist.parameters = new_parameters
        return(new_netlist)

    def write_file(self):
        """Writes the lines to the netlist file"""
        file = open(self.name,"w")
        for line in self.lines:
            file.write(line+"\n")
        file.close()

    def run_sim(self):
        # Running the simulation
        # This generates a raw file with the same name as the given file
        run_command ="..\ltspice\scad3.exe -b -Run {} $1".format(self.name)
        subprocess.call(run_command)

    def convert_raw(self, voltage_tocolect):
        extract_output="..\ltspice\ltsputil -xo1 {}.raw {}.txt %16.9e \";\" \"DATA\" time V({})".format(self.name.split(".net")[0], self.name.split(".net")[0],voltage_tocolect)
        #print extract_output
        devnull = open(os.devnull, 'w') #supress output
        subprocess.call(extract_output, stdout=devnull)

#----------------------------------------Finding parameter and value in the line----------------------------------------#{{{

def parameter_check(line):
    """finds the parameter and value for a given line and parameter"""
    regex_string = "\S+ *= *\S+" ## Regex string to identify pattern corresponding to .param statements
    param_regex = re.compile(regex_string,re.IGNORECASE)
    param_find = param_regex.findall(line) ## findall(line) matches all the instances of the regex_string pattern in line
    if param_find != []:
        return_list = []
        for a_param in param_find:
            a_param = a_param.strip("+")
            a_param = a_param.replace(" ","")
            a_param = a_param.split("=")
            variable = a_param[0]
            value = a_param[1]
            parameter_statement = parameter_statement_class(variable,value)
            return_list.append(parameter_statement)
        return(return_list) # return  list would be something like this: [Vsource=5v, Rload0=1k, Rload1=5k]
    else:
        return(False)

#--------------------------------------END Finding parameter in line----------------------------------------#}}}


#----------------------------------------Finding voltage source in the line----------------------------------------#{{{

def voltage_check(line):
    """finds the parameter and value for a given line and parameter"""
    regex_string = "V\S+ +\S+ +\S+ +PWL *\(.*\)"
    # [V]\S+ +\S+ +\S+ +[PWL] *[file] *= *\S+
    volt_regex = re.compile(regex_string,re.IGNORECASE)
    volt_find = volt_regex.findall(line) # findall(line) matches all the instances of the regex_string pattern in line
    if volt_find != []:
        volt_return_list = []
        for v_param in volt_find:
            # v_param = v_param.strip("+")
            # v_param = v_param.replace(" ","")
            v_param = v_param.split("(")
            temp=re.compile(r"voltage_\S*")
            temp2=temp.search(v_param[0])
            if  temp2:
                variable = temp2.group()
                value = v_param[1]
                value= "({}".format(value)
                voltage_statement = parameter_statement_class(variable,value)
                volt_return_list.append(voltage_statement)
        return(volt_return_list)
    else:
        return(False)

#--------------------------------------END Finding voltage source in line----------------------------------------#}}}


class parameter_statement_class:
    """A container class for carrying around parameter statement info"""
    def __init__(self,variable,value,line_number=None):
        self.variable = variable
        self.value = str(value)
        self.line_number = line_number
    def __repr__(self):
        return("{}={}".format(self.variable,self.value))
    def __str__(self):
        return("{}={}".format(self.variable,self.value))

#----------------------------------------Change Parameter Value----------------------------------------#{{{1
def change_parameter_value(a_param,new_value,current_lines):
    """changes the value of the given parameter to the given new value in the current_lines list.
        Returns the new_lines list with the parameter changed"""
    current_line = current_lines[a_param.line_number]
    current_param_statment = find_given_param_statement(a_param.variable,current_line)
    new_param_statement = a_param.variable + "=" + str(new_value)
    new_line = current_line.replace(current_param_statment,new_param_statement)
    new_lines = current_lines
    new_lines[a_param.line_number] = new_line
    return(new_lines)

def change_volt_parameter_value(a_param, new_value, current_lines):
    """changes the value of the given voltage parameter to the given new value in the current_lines list.
        Returns the new_lines list with the parameter changed"""
    current_line = current_lines[a_param.line_number]
    current_volt_param_statement = find_given_volt_param_statement (a_param.variable, current_line)
    new_param_statement = "PWL" + str(new_value)
    new_line = current_line.replace(current_volt_param_statement,new_param_statement)
    new_lines = current_lines
    new_lines[a_param.line_number] = new_line
    return(new_lines)
#----------------------------------------END Change Parameter Value----------------------------------------#}}}
#----------------------------------------Find Given Param Statement----------------------------------------#{{{
def find_given_param_statement(variable,line):
    """finds the parameter statement for a given line and vairable"""
    regex_string = "{} *= *\S+".format(variable)
    param_regex = re.compile(regex_string,re.IGNORECASE)
    param_find = param_regex.findall(line)
    if param_find != []:
        return(param_find[0])
    else:
        print("error finding parameter {}".format(variable))
#----------------------------------------END Find Given Param Statemen----------------------------------------#}}}

#----------------------------------------Find Given volt_Param Statement----------------------------------------#{{{

def find_given_volt_param_statement(variable,line):
    """finds the voltage parameter statement for a given line and variable"""
    regex_string = "PWL *\(.*\)"
    volt_param_regex = re.compile(regex_string,re.IGNORECASE)
    volt_param_find = volt_param_regex.search(line)
    if volt_param_find != []:
        return(volt_param_find.group())
    else:
        print("error finding parameter {}".format(variable))

#----------------------------------------END Find Given Param Statemen----------------------------------------#}}}

#----------------------------------------Parameter Exception Class----------------------------------------#{{{1
class ParameterException(Exception):
    """An exception class for when the given parameter is not found in the file"""
    def __init__(self,given_parameter):
        self.given_parameter = given_parameter
    def __repr__(self):
        error_string = "ParameterError: No such parameter in netlist: {}".format(self.given_parameter)
        return(error_string)
    def __str__(self):
        error_string = "ParameterError: No such parameter in netlist: {}".format(self.given_parameter)
        return(error_string)
#----------------------------------------END Parameter Exception Class----------------------------------------#}}}