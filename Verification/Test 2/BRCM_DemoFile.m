% BRCM_DEMOFILE Step by step introduction to the BRCM Toolbox. Heavily commented.
% ------------------------------------------------------------------------
% This file is part of the BRCM Toolbox v1.02.
%
% The BRCM Toolbox - Building Resistance-Capacitance Modeling for Model Predictive Control.
% Copyright (C) 2013  Automatic Control Laboratory, ETH Zurich.
% 
% The BRCM Toolbox is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% The BRCM Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with the BRCM Toolbox.  If not, see <http://www.gnu.org/licenses/>.
%
% For support check www.brcm.ethz.ch.
% ------------------------------------------------------------------------

tic;

clc;
close all;
clear all;

% This controls the output to the Command Window
   % g_debugLvl = -1 all output silent
   % g_debugLvl = 0 any not specifically requested output is completely silent
   % g_debugLvl = 1 only most important messages
   % g_debugLvl = 2 all messages

global g_debugLvl
g_debugLvl = 1;


BRCMRootPath = getBRCMRootPath();
cd(BRCMRootPath);

demoBuilding = 'DemoBuilding';

thermalModelDataDir =   [BRCMRootPath,filesep,'BuildingData',filesep,demoBuilding,filesep,'ThermalModel'];
EHFModelDataDir =       [BRCMRootPath,filesep,'BuildingData',filesep,demoBuilding,filesep,'EHFM'];





%% --------------------------------------------------------------------------------------
% 1) Create a building
% --------------------------------------------------------------------------------------

% Create an empty Building object with an optional identifier argument.
buildingIdentifier = demoBuilding;
B = Building(buildingIdentifier);





%% --------------------------------------------------------------------------------------
% 2) Load the thermal model data
% --------------------------------------------------------------------------------------

% Load the thermal model data. 
B.loadThermalModelData(thermalModelDataDir);

% The thermal model data consists of zones, building elements, constructions,
% materials, windows and parameters. The data of each element group must
% be provided by a separate .xls files and all base files are required for 
% loading the builing data. We require the file names and the file contents to follow a
% specific convention, see the Documentation.





%% --------------------------------------------------------------------------------------
% 3) Declare external heat flux models that should be included
% --------------------------------------------------------------------------------------

% Heat exchange with ambient air and solar gains
EHFModelClassFile = 'BuildingHull.m';                                         % This is the m-file defining this EHF model's class.
EHFModelDataFile = [EHFModelDataDir,filesep,'buildinghull'];                  % This is the spreadsheet containing this EHF model's specification.
EHFModelIdentifier = 'BuildingHull';                                          % This string identifies the EHF model uniquely
B.declareEHFModel(EHFModelClassFile,EHFModelDataFile,EHFModelIdentifier);

% Radiators
EHFModelClassFile = 'Radiators.m'; 
EHFModelDataFile = [EHFModelDataDir,filesep,'radiators']; 
EHFModelIdentifier = 'Rad';
B.declareEHFModel(EHFModelClassFile,EHFModelDataFile,EHFModelIdentifier);



%% --------------------------------------------------------------------------------------
% 4) Display thermal model data to Command Window and draw Building (optional) 
% --------------------------------------------------------------------------------------

% Print the thermal model data in the Command Window for an overview
B.printThermalModelData;

% 3-D plot of Building
% % % B.drawBuilding;

% It is possible to control the labeling
% B.drawBuilding('NoBELabels');
% B.drawBuilding('NoLabels');
% B.drawBuilding('NoZoneLabels');

% % % % 2-D plot of Building
% % % B.drawBuilding('Floorplan');
% % % 
% % % % Drawing parts of the building can be done by a cell array of zone group and/or zone identifiers
% % % B.drawBuilding({'Z0001'}); 


%% --------------------------------------------------------------------------------------
% 5) Manipulate thermal model data and save it back to disk (optional) 
% --------------------------------------------------------------------------------------

% All(many) of the parameters can be retrieved(modified) by using the ThermalModelData's setValue(getValue) method 

% One can set numerical values ...
thermalModelDataIdentifier = 'M0001';
thermalModelDataProperty = 'specific_heat_capacity';
specHeatCapacity = B.thermal_model_data.getValue(thermalModelDataIdentifier,thermalModelDataProperty);
B.thermal_model_data.setValue(thermalModelDataIdentifier,thermalModelDataProperty,specHeatCapacity*1.2);

% % % % ... or parameter identifiers
% % % thermalModelDataIdentifier = 'W0001';
% % % thermalModelDataProperty = 'U_value';
% % % B.thermal_model_data.setValue(thermalModelDataIdentifier,thermalModelDataProperty,'UValue_Window_EPConstr_WindowGlazing_Office');
% % % 
% % % 
% % % % Write to disk
% % % modifiedThermalModelDataDir = [thermalModelDataDir,'_mod'];
% % % forceFlag = false;                     % optional flag for automatic overwriting of existing files (default = false)
% % % writeToCSV = false;                    % optional flag controlling output format. Default is .xls.
% % % % B.writeThermalModelData(modifiedThermalModelDataDir,forceFlag,writeToCSV)


% Reload the old data to continue
B.loadThermalModelData(thermalModelDataDir); 







%% --------------------------------------------------------------------------------------
% 6) Generate thermal model and full model
% --------------------------------------------------------------------------------------

% Generate thermal model (optional)
B.generateThermalModel;

% Generate (full) building model (includes thermal model generation if not yet done)
B.generateBuildingModel;

% Display all available identifiers (these are the names of the control inputs / disturbances / states in the same order as they appear in the matrices)
B.building_model.printIdentifiers;

% Disretization
Ts_hrs = 2/60 ;% every 2 minutes
B.building_model.setDiscretizationStep(Ts_hrs);
B.building_model.discretize();







%% --------------------------------------------------------------------------------------
% 7) Retrieve Matrices and generate costs and constraints
% --------------------------------------------------------------------------------------

% Access of full model matrices
discreteTimeFullModelMatrix_A = B.building_model.discrete_time_model.A; % same for Bu,Bv,Bvu,Bxu
continuousTimeFullModelMatrix_A = B.building_model.continuous_time_model.A; % same for Bu,Bv,Bvu,Bxu

% Access of thermal model matrices
continuousTimeThermalModelMatrix_A = B.building_model.thermal_submodel.A; % same for Bq
[discreteTimeThermalModelMatrix_A,discreteTimeThermalModelMatrix_Bq] = B.building_model.thermal_submodel.discretize(Ts_hrs); % these are usually not used, hence not stored

% Access of EHF model matrices (here for the first EHF model)
EHFM1Matrix_Aq = B.building_model.EHF_submodels{1}.Aq; % same for Bq_u, Bq_v, Bq_vu, Bq_xu


% Get constraint matrices such that % Fx*x+Fu*u+Fv*v <= g. These are the constraints for one particular set of potentially 
% time-varying constraintsParameters. Every row of the matrices represents one constraint the name of which is the 
% corresponding entry in constraint_identifiers. The parameters that have to be passed must be in the form 
% constraintsParameters.<EHF_identifier>.<parameters>. Check the documentation to learn which <parameters> are necessary for
% a particular EHF model.


constraintsParameters = struct();

constraintsParameters.AHU1.mdot_min = 0;
constraintsParameters.AHU1.mdot_max = 1;
constraintsParameters.AHU1.T_supply_max = 30;
constraintsParameters.AHU1.T_supply_min = 22;
constraintsParameters.AHU1.Q_heat_min = 0;
constraintsParameters.AHU1.Q_heat_max = 1000;
constraintsParameters.AHU1.Q_cool_min = 0;
constraintsParameters.AHU1.Q_cool_max = 1;
constraintsParameters.AHU1.x = 23*ones(length(B.building_model.identifiers.x),1);
constraintsParameters.AHU1.v_fullModel = 20*ones(length(B.building_model.identifiers.v),1);

constraintsParameters.BuildingHull.BPos_blinds_E_min = 0.1;
constraintsParameters.BuildingHull.BPos_blinds_E_max = 1;
constraintsParameters.BuildingHull.BPos_blinds_L_min = 0.1;
constraintsParameters.BuildingHull.BPos_blinds_L_max = 1;
constraintsParameters.BuildingHull.BPos_blinds_N_min = 0.1;
constraintsParameters.BuildingHull.BPos_blinds_N_max = 1;
constraintsParameters.BuildingHull.BPos_blinds_S_min = 0.1;
constraintsParameters.BuildingHull.BPos_blinds_S_max = 1;
constraintsParameters.BuildingHull.BPos_blinds_W_min = 0.1;
constraintsParameters.BuildingHull.BPos_blinds_W_max = 1;


constraintsParameters.TABS.Q_BEH_hTABS_heat_min = 100;
constraintsParameters.TABS.Q_BEH_hTABS_heat_max = 1000;
constraintsParameters.TABS.Q_BEH_cTABS_cool_min = 100;
constraintsParameters.TABS.Q_BEH_cTABS_cool_max = 1000;

constraintsParameters.Rad.Q_rad_CornerOffices_min = 1;
constraintsParameters.Rad.Q_rad_CornerOffices_max = 3;
constraintsParameters.Rad.Q_rad_Offices_min = 2;
constraintsParameters.Rad.Q_rad_Offices_max = 4;

[Fx,Fu,Fv,g] = B.building_model.getConstraintsMatrices(constraintsParameters);


% Get cost vector such that J = cu*u. This is the cost for one particular set of potentially 
% time-varying costParameters. The parameters that have to be passed must be in the form 
% costParameters.<EHF_identifier>.<parameters>. Check the documentation to learn which <parameters> are necessary for
% a particular EHF model. 

costParameters = struct();
costParameters.Rad.costPerJouleHeated = 10;
costParameters.TABS.costPerJouleCooled = 10;
costParameters.TABS.costPerJouleHeated = 10;
costParameters.AHU1.costPerKgAirTransported = 1;
costParameters.AHU1.costPerJouleCooled = 10;
costParameters.AHU1.costPerKgCooledByEvapCooler = 10;
costParameters.AHU1.costPerJouleHeated = 10;
cu = B.building_model.getCostVector(costParameters);

% If the building_model B.building_model should be saved to use the model in another place, it is necessary that the Classes folder 
% is on the path, otherwise the saved data can not be loaded correctly. If only the matrices are needed, then just 
% the B.building_model.discrete_time_model should be saved and the Classes folder is not necessary.

%% --------------------------------------------------------------------------------------
% 8) Discrete-time simulation of the thermal model or the building model (here only shown for the building model)
% --------------------------------------------------------------------------------------

% The class SimulationExperiment provides a simulation environment. An instantiation requires
% a Building object that at least containts a thermal model and a the sampling time (this will be 
% also the simulation timestep). Once the SimulationExperiment object is instantiated, its building 
% object in  object cannot be manipulated anymore. 

SimExp = SimulationExperiment(B);


% It is possible to print/access the identifiers and to access the model/simulation time step
SimExp.printIdentifiers();
identifiers = SimExp.getIdentifiers();
len_v = length(identifiers.v);
len_u = length(identifiers.u);
len_x = length(identifiers.x);
Ts_hrs = SimExp.getSamplingTime();

% Number of simulation time steps and initial condition must be set before the simulation
n_timeSteps = 24*30-2;
SimExp.setNumberOfSimulationTimeSteps(n_timeSteps);
% x0 = 0*ones(len_x,1);
x0(1,1)=9;
x0(2,1)=9; % ELB 239
x0(3,1)=9; % ELB 241
x0(4,1)=9; % ELB 216
x0(5,1)=9;
SimExp.setInitialState(x0);

% Simulation of the building model. The simulation environment allows 2 different simulation modes.
%    1)   mode 'inputTrajectory':     Simulate the model with an input sequence provided by the user.
%    2)   mode 'handle':              Simulate the model with an input sequence provided by a function handle associated with a function provided by the user.

mode = 1;

switch mode

   case 1
      % set up input and disturbance sequence
      V = zeros(len_v,n_timeSteps);
      U = zeros(len_u,n_timeSteps);
%       idx_u_rad_Offices = getIdIndex('u_rad_Offices',identifiers.u);
      idx_v_Tamb = getIdIndex('v_Tamb',identifiers.v);
%       idx_v_ELB239 = getIdIndex('v_ELB239',identifiers.v);
%       idx_v_ELB241 = getIdIndex('v_ELB241',identifiers.v);
%       idx_v_ELB216 = getIdIndex('v_ELB216',identifiers.v)
      V(idx_v_Tamb,:) = [9.0, 9.0, 9.0, 9.129, 9.3, 9.257, 9.2, 9.243, 9.3, 9.257, 9.2, 9.243, 9.3, 9.3, 9.3, 9.3, 9.3, 9.3, 9.3, 9.257, 9.2, 9.243, 9.3, 9.3, 9.3, 9.428, 9.6, 9.6, 9.6, 9.387, 9.1, 9.1, 9.1, 9.1, 9.1, 9.185, 9.3, 9.257, 9.2, 9.2, 9.2, 9.115, 9.0, 8.915, 8.8, 8.885, 9.0, 8.915, 8.8, 8.715, 8.6, 8.6, 8.6, 8.856, 9.2, 8.944, 8.6, 8.6, 8.6, 8.685, 8.8, 8.757, 8.7, 8.871, 9.1, 8.972, 8.8, 8.8, 8.8, 8.928, 9.1, 9.015, 8.9, 8.9, 8.9, 8.985, 9.1, 8.973, 8.8, 8.8, 8.8, 8.8, 8.8, 8.8, 8.8, 8.8, 8.8, 8.8, 8.8, 8.8, 8.8, 8.969, 9.2, 9.285, 9.4, 9.4, 9.4, 9.315, 9.2, 9.2, 9.2, 9.2, 9.2, 9.2, 9.2, 9.327, 9.5, 9.374, 9.2, 9.242, 9.3, 9.174, 9.0, 9.084, 9.2, 9.326, 9.5, 9.374, 9.2, 9.2, 9.2, 9.284, 9.4, 9.4, 9.4, 9.316, 9.2, 9.2, 9.2, 9.2, 9.2, 9.2, 9.2, 9.242, 9.3, 9.3, 9.3, 9.258, 9.2, 9.2, 9.2, 9.2, 9.2, 9.242, 9.3, 9.425, 9.6, 9.434, 9.2, 9.283, 9.4, 9.4, 9.4, 9.317, 9.2, 9.242, 9.3, 9.383, 9.5, 9.376, 9.2, 9.241, 9.3, 9.176, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.124, 9.3, 9.3, 9.3, 9.3, 9.3, 9.3, 9.3, 9.382, 9.5, 9.418, 9.3, 9.423, 9.6, 9.477, 9.3, 9.382, 9.5, 9.417, 9.3, 9.341, 9.4, 9.4, 9.4, 9.317, 9.2, 9.241, 9.3, 9.383, 9.5, 9.417, 9.3, 9.3, 9.3, 9.3, 9.3, 9.3, 9.3, 9.424, 9.6, 9.6, 9.6, 9.476, 9.3, 9.382, 9.5, 9.376, 9.2, 9.2, 9.2, 9.241, 9.3, 9.259, 9.2, 9.324, 9.5, 9.376, 9.2, 9.241, 9.3, 9.3, 9.3, 9.3, 9.3, 9.259, 9.2, 9.282, 9.4, 9.235, 9.0, 9.0, 9.0, 8.876, 8.7, 8.741, 8.8, 8.718, 8.6, 8.723, 8.9, 8.982, 9.1, 9.1, 9.1, 9.264, 9.5, 9.623, 9.8, 9.8, 9.8, 9.8, 9.8, 9.923, 10.1, 10.1, 10.1, 10.1, 10.1, 10.141, 10.2, 10.118, 10.0, 10.082, 10.2, 10.323, 10.5, 10.377, 10.2, 10.241, 10.3, 10.259, 10.2, 10.323, 10.5, 10.623, 10.8, 10.8, 10.8, 10.882, 11.0, 11.0, 11.0, 11.0, 11.0, 11.082, 11.2, 11.282, 11.4, 11.522, 11.7, 11.823, 12.0, 11.877, 11.7, 11.823, 12.0, 12.082, 12.2, 12.2, 12.2, 12.323, 12.5, 12.5, 12.5, 12.5, 12.5, 12.5, 12.5, 12.5, 12.5, 12.378, 12.2, 12.2, 12.2, 12.2, 12.2, 12.08, 11.9, 11.9, 11.9, 11.98, 12.1, 12.1, 12.1, 12.1, 12.1, 12.18, 12.3, 12.538, 12.9, 13.179, 13.6, 13.6, 13.6, 13.958, 14.5, 14.698, 15.0, 15.04, 15.1, 15.219, 15.4, 15.44, 15.5, 15.54, 15.6, 15.56, 15.5, 15.461, 15.4, 15.321, 15.2, 15.2, 15.2, 15.239, 15.3, 15.103, 14.8, 14.721, 14.6, 14.561, 14.5, 14.342, 14.1, 14.1, 14.1, 14.139, 14.2, 14.239, 14.3, 14.182, 14.0, 14.0, 14.0, 14.0, 14.0, 13.648, 13.1, 13.022, 12.9, 12.9, 12.9, 12.9, 12.9, 12.9, 12.9, 12.9, 12.9, 12.9, 12.9, 12.822, 12.7, 12.739, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.916, 13.1, 13.1, 13.1, 13.023, 12.9, 12.9, 12.9, 12.861, 12.8, 12.877, 13.0, 12.923, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.724, 12.6, 12.676, 12.8, 12.762, 12.7, 12.738, 12.8, 12.8, 12.8, 12.8, 12.8, 12.953, 13.2, 13.047, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.8, 12.724, 12.6, 12.6, 12.6, 12.524, 12.4, 12.4, 12.4, 12.4, 12.4, 12.324, 12.2, 12.238, 12.3, 12.186, 12.0, 12.0, 12.0, 12.076, 12.2, 12.124, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 11.812, 11.5, 11.5, 11.5, 11.462, 11.4, 11.4, 11.4, 11.513, 11.7, 11.587, 11.4, 11.4, 11.4, 11.512, 11.7, 11.625, 11.5, 11.5, 11.5, 11.388, 11.2, 11.2, 11.2, 11.2, 11.2, 11.051, 10.8, 10.8, 10.8, 10.763, 10.7, 10.7, 10.7, 10.737, 10.8, 10.763, 10.7, 10.774, 10.9, 10.826, 10.7, 10.7, 10.7, 10.7, 10.7, 10.7, 10.7, 10.737, 10.8, 10.652, 10.4, 10.4, 10.4, 10.4, 10.4, 10.51, 10.7, 10.737, 10.8, 10.763, 10.7, 10.737, 10.8, 10.763, 10.7, 10.773, 10.9, 10.827, 10.7, 10.773, 10.9, 10.9, 10.9, 10.863, 10.8, 10.8, 10.8, 10.946, 11.2, 11.091, 10.9, 10.9, 10.9, 10.973, 11.1, 11.1, 11.1, 11.1, 11.1, 11.1, 11.1, 11.1, 11.1, 11.064, 11.0, 11.0, 11.0, 11.036, 11.1, 11.028, 10.9, 10.9, 10.9, 10.9, 10.9, 10.828, 10.7, 10.736, 10.8, 10.692, 10.5, 10.5, 10.5, 10.428, 10.3, 10.479, 10.8, 10.657, 10.4, 10.4, 10.4, 10.4, 10.4, 10.543, 10.8, 10.728, 10.6, 10.6, 10.6, 10.6, 10.6, 10.6, 10.6, 10.6, 10.6, 10.707, 10.9, 10.829, 10.7, 10.736, 10.8, 10.729, 10.6, 10.6, 10.6, 10.6, 10.6, 10.6, 10.6, 10.6, 10.6, 10.6, 10.6, 10.458, 10.2, 10.306, 10.5, 10.5, 10.5, 10.43, 10.3, 10.265, 10.2, 10.2, 10.2, 10.2, 10.2, 10.27, 10.4, 10.4, 10.4, 10.33, 10.2, 10.2, 10.2, 10.2, 10.2, 10.2, 10.2, 10.2, 10.2, 10.235, 10.3, 10.195, 10.0, 10.0, 10.0, 9.93, 9.8, 9.87, 10.0, 9.896, 9.7, 9.909, 10.3, 10.195, 10.195, 10.195, 10.195, 10.195]; % ambient temperature to constant 2
%       V(idx_v_ELB239,:) = [22.1, 22.1, 22.1, 22.1, 22.1, 22.037, 22.0, 22.063, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.037, 22.0, 22.0, 22.0, 22.063, 22.1, 22.037, 22.0, 22.125, 22.2, 22.2, 22.2, 22.075, 22.0, 22.063, 22.1, 22.1, 22.1, 22.038, 22.0, 22.0, 22.0, 22.0, 22.0, 22.125, 22.2, 22.075, 22.0, 22.062, 22.1, 22.038, 22.0, 22.063, 22.1, 22.1, 22.1, 22.037, 22.0, 21.937, 21.9, 21.963, 22.0, 22.0, 22.0, 22.0, 22.0, 22.0, 22.0, 21.937, 21.9, 21.963, 22.0, 22.063, 22.1, 22.1, 22.1, 22.038, 22.0, 22.0, 22.0, 22.0, 22.0, 22.0, 22.0, 22.0, 22.0, 21.938, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 22.024, 22.1, 21.976, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 22.024, 22.1, 21.976, 21.9, 21.9, 21.9, 21.9, 21.9, 21.962, 22.0, 21.876, 21.8, 21.8, 21.8, 21.8, 21.8, 21.924, 22.0, 21.876, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.923, 22.0, 21.938, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.961, 22.0, 22.061, 22.1, 21.916, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.922, 22.0, 22.0, 22.0, 21.939, 21.9, 21.9, 21.9, 21.778, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.823, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.777, 21.7, 21.7, 21.7, 21.823, 21.9, 21.777, 21.7, 21.761, 21.8, 21.8, 21.8, 21.8, 21.8, 21.739, 21.7, 21.761, 21.8, 21.739, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.884, 22.0, 21.816, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.761, 21.8, 21.8, 21.8, 21.8, 21.8, 21.739, 21.7, 21.761, 21.8, 21.677, 21.6, 21.661, 21.7, 21.7, 21.7, 21.7, 21.7, 21.761, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.739, 21.7, 21.7, 21.7, 21.761, 21.8, 21.739, 21.7, 21.7, 21.7, 21.822, 21.9, 21.778, 21.7, 21.7, 21.7, 21.761, 21.8, 21.739, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.822, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.9, 21.961, 22.0, 21.878, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.8, 21.92, 22.0, 21.88, 21.8, 21.74, 21.7, 21.76, 21.8, 21.8, 21.8, 21.8, 21.8, 21.92, 22.0, 22.06, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.339, 22.5, 22.56, 22.6, 22.48, 22.4, 22.46, 22.5, 22.381, 22.3, 22.3, 22.3, 22.3, 22.3, 22.3, 22.3, 22.3, 22.3, 22.3, 22.3, 22.3, 22.3, 22.3, 22.3, 22.419, 22.5, 22.381, 22.3, 22.3, 22.3, 22.3, 22.3, 22.3, 22.3, 22.241, 22.2, 22.259, 22.3, 22.241, 22.2, 22.319, 22.4, 22.4, 22.4, 22.4, 22.4, 22.4, 22.4, 22.4, 22.4, 22.222, 22.1, 22.041, 22.0, 22.059, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.218, 22.3, 22.241, 22.2, 22.2, 22.2, 22.318, 22.4, 22.4, 22.4, 22.282, 22.2, 22.2, 22.2, 22.259, 22.3, 22.3, 22.3, 22.241, 22.2, 22.259, 22.3, 22.241, 22.2, 22.317, 22.4, 22.283, 22.2, 22.2, 22.2, 22.141, 22.1, 22.1, 22.1, 22.218, 22.3, 22.182, 22.1, 22.218, 22.3, 22.182, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.1, 22.217, 22.3, 22.241, 22.2, 22.2, 22.2, 22.2, 22.2, 22.2, 22.2, 22.083, 22.0, 22.0, 22.0, 22.0, 22.0, 22.059, 22.1, 22.041, 22.0, 22.058, 22.1, 22.1, 22.1, 22.042, 22.0, 22.058, 22.1, 21.983, 21.9, 22.075, 22.2, 22.083, 22.0, 21.942, 21.9, 21.958, 22.0, 21.942, 21.9, 21.9, 21.9, 21.958, 22.0, 21.942, 21.9, 21.9, 21.9, 21.958, 22.0, 21.942, 21.9, 21.958, 22.0, 22.0, 22.0, 21.884, 21.8, 21.8, 21.8, 21.742, 21.7, 21.7, 21.7, 21.7, 21.7, 21.758, 21.8, 21.8, 21.8, 21.684, 21.6, 21.6, 21.6, 21.6, 21.6, 21.6, 21.6, 21.6, 21.6, 21.715, 21.8, 21.742, 21.7, 21.758, 21.8, 21.627, 21.5, 21.558, 21.6, 21.6, 21.6, 21.715, 21.8, 21.685, 21.6, 21.773, 21.9, 21.9, 21.9, 21.785, 21.7, 21.757, 21.8, 21.8, 21.8, 21.8, 21.8, 21.685, 21.6, 21.715, 21.8, 21.628, 21.5, 21.615, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.7, 21.643, 21.6, 21.6, 21.6, 21.6, 21.6, 21.714, 21.8, 21.8, 21.8, 21.629, 21.5, 21.557, 21.6, 21.6, 21.6, 21.771, 21.9, 21.786, 21.7, 21.757, 21.8, 21.686, 21.6, 21.6, 21.6, 21.543, 21.5, 21.614, 21.7, 21.7, 21.7, 21.7, 21.7, 21.586, 21.5, 21.5, 21.5, 21.5, 21.5, 21.557, 21.6, 21.6, 21.6, 21.6, 21.6, 21.543, 21.5, 21.5, 21.5, 21.5, 21.5, 21.5, 21.5, 21.443, 21.4, 21.457, 21.5, 21.5, 21.5, 21.613, 21.7, 21.531, 21.4, 21.513, 21.6, 21.544, 21.5, 21.444, 21.4, 21.456, 21.5, 21.556, 21.6, 21.487, 21.4, 21.4, 21.4, 21.4, 21.4, 21.512, 21.6, 21.6, 21.6, 21.488, 21.4, 21.512, 21.6, 21.544, 21.5, 21.556, 21.6, 21.488, 21.4, 21.4, 21.4, 21.4, 21.4, 21.568, 21.568, 21.568, 21.568, 21.568, 21.568, 21.568, 21.568, 21.568]; % ambient temperature to constant 22
%       V(idx_v_ELB241,:) = [22.7, 22.7, 23.076, 23.2, 22.899, 22.8, 22.8, 22.8, 23.025, 23.1, 22.875, 22.8, 23.025, 23.1, 23.025, 23.0, 22.85, 22.8, 22.95, 23.0, 22.775, 22.7, 22.775, 22.8, 22.8, 22.8, 22.725, 22.7, 22.925, 23.0, 23.0, 23.0, 22.85, 22.8, 23.024, 23.1, 23.1, 23.1, 23.249, 23.3, 23.076, 23.0, 22.925, 22.9, 23.049, 23.1, 23.025, 23.0, 22.925, 22.9, 22.9, 22.9, 22.9, 22.9, 22.975, 23.0, 22.85, 22.8, 22.95, 23.0, 22.925, 22.9, 23.125, 23.2, 23.05, 23.0, 23.0, 23.0, 22.925, 22.9, 22.975, 23.0, 23.0, 23.0, 23.0, 23.0, 23.224, 23.3, 23.076, 23.0, 23.075, 23.1, 23.1, 23.1, 22.951, 22.9, 22.9, 22.9, 22.975, 23.0, 22.925, 22.9, 22.9, 22.9, 23.123, 23.2, 22.902, 22.8, 22.949, 23.0, 22.851, 22.8, 22.949, 23.0, 22.851, 22.8, 22.8, 22.8, 22.949, 23.0, 22.926, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 23.048, 23.1, 22.878, 22.8, 22.8, 22.8, 22.948, 23.0, 23.074, 23.1, 22.878, 22.8, 22.8, 22.8, 22.8, 22.8, 23.096, 23.2, 22.904, 22.8, 22.8, 22.8, 22.948, 23.0, 22.852, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.726, 22.7, 22.774, 22.8, 22.726, 22.7, 22.774, 22.8, 23.021, 23.1, 23.026, 23.0, 23.0, 23.0, 23.147, 23.2, 23.053, 23.0, 22.927, 22.9, 23.12, 23.2, 23.053, 23.0, 23.0, 23.0, 22.927, 22.9, 22.9, 22.9, 22.9, 22.9, 23.047, 23.1, 22.953, 22.9, 22.974, 23.0, 23.147, 23.2, 23.053, 23.0, 23.0, 23.0, 22.926, 22.9, 22.9, 22.9, 23.121, 23.2, 23.053, 23.0, 23.147, 23.2, 23.053, 23.0, 23.147, 23.2, 23.2, 23.2, 23.053, 23.0, 23.074, 23.1, 23.026, 23.0, 23.0, 23.0, 23.074, 23.1, 23.247, 23.3, 23.079, 23.0, 23.074, 23.1, 23.1, 23.1, 23.1, 23.1, 23.247, 23.3, 23.006, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 22.753, 22.7, 22.847, 22.9, 22.973, 23.0, 23.073, 23.1, 22.733, 22.6, 22.747, 22.8, 23.02, 23.1, 22.806, 22.7, 22.7, 22.7, 22.847, 22.9, 22.753, 22.7, 22.7, 22.7, 22.847, 22.9, 22.753, 22.7, 22.773, 22.8, 22.8, 22.8, 22.727, 22.7, 22.7, 22.7, 22.847, 22.9, 22.9, 22.9, 22.9, 22.9, 23.047, 23.1, 22.88, 22.8, 22.8, 22.8, 23.167, 23.3, 23.08, 23.0, 23.22, 23.3, 23.08, 23.0, 22.927, 22.9, 22.973, 23.0, 23.22, 23.3, 23.08, 23.0, 23.22, 23.3, 23.08, 23.0, 22.927, 22.9, 22.9, 22.9, 22.973, 23.0, 23.293, 23.4, 23.036, 22.9, 23.045, 23.1, 22.955, 22.9, 23.045, 23.1, 23.1, 23.1, 22.955, 22.9, 22.9, 22.9, 22.972, 23.0, 23.072, 23.1, 23.028, 23.0, 23.144, 23.2, 23.2, 23.2, 23.272, 23.3, 23.444, 23.5, 23.212, 23.1, 23.1, 23.1, 23.028, 23.0, 23.072, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 23.46, 23.6, 23.528, 23.5, 23.428, 23.4, 23.4, 23.4, 23.256, 23.2, 23.487, 23.6, 23.6, 23.6, 23.313, 23.2, 23.2, 23.2, 23.343, 23.4, 23.257, 23.2, 23.2, 23.2, 23.272, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.229, 23.2, 23.2, 23.2, 23.2, 23.2, 23.414, 23.5, 23.286, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.485, 23.6, 23.457, 23.4, 23.186, 23.1, 23.242, 23.3, 23.158, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 23.242, 23.3, 23.3, 23.3, 23.229, 23.2, 23.271, 23.3, 23.158, 23.1, 23.313, 23.4, 23.187, 23.1, 23.1, 23.1, 23.029, 23.0, 23.212, 23.3, 23.017, 22.9, 22.9, 22.9, 23.042, 23.1, 23.384, 23.5, 23.216, 23.1, 22.958, 22.9, 23.113, 23.2, 22.987, 22.9, 22.9, 22.9, 23.113, 23.2, 23.058, 23.0, 23.284, 23.4, 23.187, 23.1, 23.1, 23.1, 23.1, 23.1, 22.958, 22.9, 22.9, 22.9, 23.042, 23.1, 22.958, 22.9, 22.971, 23.0, 22.929, 22.9, 22.9, 22.9, 23.112, 23.2, 23.2, 23.2, 22.988, 22.9, 22.971, 23.0, 23.071, 23.1, 23.029, 23.0, 23.0, 23.0, 23.0, 23.0, 23.0, 23.0, 23.0, 23.0, 23.0, 23.0, 23.07, 23.1, 23.03, 23.0, 23.0, 23.0, 23.211, 23.3, 23.089, 23.0, 23.351, 23.5, 23.149, 23.0, 23.0, 23.0, 23.07, 23.1, 23.03, 23.0, 22.93, 22.9, 23.04, 23.1, 22.96, 22.9, 23.11, 23.2, 22.99, 22.9, 22.97, 23.0, 22.93, 22.9, 22.9, 22.9, 22.9, 22.9, 22.97, 23.0, 22.86, 22.8, 23.149, 23.3, 23.021, 22.9, 22.76, 22.7, 22.77, 22.8, 22.8, 22.8, 22.94, 23.0, 22.791, 22.7, 22.7, 22.7, 22.77, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 22.8, 23.008, 23.1, 23.031, 23.0, 22.931, 22.9, 23.039, 23.1, 22.961, 22.9, 22.969, 23.0, 22.931, 22.9, 23.177, 23.3, 23.231, 23.2, 22.992, 22.9, 23.246, 23.4, 23.469, 23.5, 23.154, 23.0, 23.0, 23.0, 23.0, 23.0, 22.931, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 22.9, 22.969, 23.0, 22.862, 22.8, 22.8, 22.8, 22.8, 22.8, 22.731, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.7, 22.631, 22.6, 22.6, 22.6, 22.6, 22.6, 22.6, 22.6, 22.6, 22.6, 22.532, 22.5, 22.568, 22.6, 22.6, 22.6, 22.6, 22.6, 22.6, 22.6, 22.532, 22.5, 22.568, 22.6, 22.6, 22.6, 22.6, 22.6, 22.6, 22.6, 22.804, 22.9, 22.968, 23.0, 22.932, 22.9, 22.764, 22.7, 22.7, 22.7, 22.972, 23.1, 22.896, 22.896, 22.896, 22.896]; % ambient temperature to constant 22
%       V(idx_v_ELB216,:) = [23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.364, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.264, 23.2, 23.272, 23.4, 23.4, 23.4, 23.4, 23.4, 23.328, 23.2, 23.272, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.328, 23.2, 23.272, 23.4, 23.328, 23.2, 23.272, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.328, 23.2, 23.272, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.364, 23.3, 23.371, 23.5, 23.429, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.335, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.435, 23.5, 23.465, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.365, 23.3, 23.37, 23.5, 23.43, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.265, 23.2, 23.269, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.331, 23.2, 23.235, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.369, 23.5, 23.465, 23.4, 23.435, 23.5, 23.431, 23.3, 23.3, 23.3, 23.3, 23.3, 23.369, 23.5, 23.431, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.369, 23.5, 23.465, 23.4, 23.434, 23.5, 23.466, 23.4, 23.434, 23.5, 23.431, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.267, 23.2, 23.233, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.367, 23.5, 23.433, 23.3, 23.367, 23.5, 23.433, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.366, 23.5, 23.5, 23.5, 23.467, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.367, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.267, 23.2, 23.233, 23.3, 23.267, 23.2, 23.2, 23.2, 23.265, 23.4, 23.4, 23.4, 23.335, 23.2, 23.2, 23.2, 23.232, 23.3, 23.268, 23.2, 23.2, 23.2, 23.2, 23.2, 23.232, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.3, 23.268, 23.2, 23.232, 23.3, 23.268, 23.2, 23.232, 23.3, 23.268, 23.2, 23.232, 23.3, 23.268, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.232, 23.3, 23.268, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.232, 23.3, 23.3, 23.3, 23.268, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.2, 23.168, 23.1, 23.1, 23.1, 23.1, 23.1, 23.164, 23.3, 23.3, 23.3, 23.3, 23.3, 23.268, 23.2, 23.2, 23.2, 23.232, 23.3, 23.268, 23.2, 23.232, 23.3, 23.268, 23.2, 23.2, 23.2, 23.2, 23.2, 23.232, 23.3, 23.268, 23.2, 23.263, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.4, 23.431, 23.5, 23.469, 23.4, 23.4, 23.4, 23.431, 23.5, 23.469, 23.4, 23.431, 23.5, 23.5, 23.5, 23.5, 23.5, 23.531, 23.6, 23.569, 23.5, 23.5, 23.5, 23.531, 23.6, 23.6, 23.6, 23.569, 23.5, 23.531, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.569, 23.5, 23.531, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.6, 23.661, 23.8, 23.8, 23.8, 23.77, 23.7, 23.7, 23.7, 23.73, 23.8, 23.739, 23.6, 23.6, 23.6, 23.6, 23.6, 23.661, 23.8, 23.8, 23.8, 23.77, 23.7, 23.7, 23.7, 23.7, 23.7, 23.67, 23.6, 23.6, 23.6, 23.6, 23.6, 23.66, 23.8, 23.77, 23.7, 23.7, 23.7, 23.73, 23.8, 23.77, 23.7, 23.7, 23.7, 23.7, 23.7, 23.7, 23.7, 23.7, 23.7, 23.73, 23.8, 23.74, 23.6, 23.6, 23.6, 23.63, 23.7, 23.73, 23.8, 23.741, 23.6, 23.6, 23.6, 23.659, 23.8, 23.741, 23.6, 23.6, 23.6, 23.659, 23.8, 23.8, 23.8, 23.8, 23.8, 23.77, 23.7, 23.7, 23.7, 23.7, 23.7, 23.7, 23.7, 23.7, 23.7, 23.729, 23.8, 23.771, 23.7, 23.7, 23.7, 23.759, 23.9, 23.841, 23.7, 23.729, 23.8, 23.8, 23.8, 23.771, 23.771, 23.771];
%       
%       U(idx_u_rad_Offices,5:end) = 5; % step of 5 W/m2 in the office radiators from the 5th timestep on
      
      [X,U,V,t_hrs] = SimExp.simulateBuildingModel('inputTrajectory',U,V);
      
   case 2
      [X,U,V,t_hrs] = SimExp.simulateBuildingModel('handle',@demoUVGenerator);
      
   otherwise
      error('Bad mode.');
      
end


% Plot the simulation results. Every call to SimExp.plot generates a separate figure
% The command takes a cell array of cells. Every cell produces a subplot for every
% identifier contained in it.

firstFigure{1} = identifiers.x(1:5);         % any state/input/disturbance identifier can be used here
firstFigure{2} = {'Z0001'};          % zone identifiers work too
% % % firstFigure{3} = {'ZoneGrp_East','Z0010'};   % zone group identifiers work too
fh1 = SimExp.plot(firstFigure);

secondFigure{1} = identifiers.u;         
secondFigure{2} = identifiers.v;          
fh2 = SimExp.plot(secondFigure);

sim_temperature=X(1,:)';
toc

